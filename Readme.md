- [Structure of file .irp (Iridium)](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-structure-of-file-irp-iridium)
    - [Panel](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-panel)
    - [StartUp](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-startup)
    - [Devices](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-devices)
    - [UserDeviceTree](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-userdevicetree)
    - [UniversalTree](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-universaltree)
    - [Images](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-images)
    - [Icons](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-icons)
    - [Fonts](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-fonts)
    - [Sounds](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-sounds)
    - [Scripts](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-scripts)
    - [ScriptModules](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-scriptmodules)
    - [RelationsTags](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-relationstags)
    - [Effects](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-effects)
    - [Pages](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-pages)
    - [Popups](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-popups)
    - [Folders](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-folders)
    - [VirtualTags](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-virtualtags)
    - [Macros](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-macros)
- [Project Example (.irpz-file)](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-project-example-irpz-file)
- [Project Example in XML (.irp-file)](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-project-example-in-xml-irp-file)
- [Copyrights](https://bitbucket.org/estbeetoo/irp_structure/overview#markdown-header-copyrights)

# Structure of file .irp (Iridium)
```
<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<Panel>
    <StartUp></StartUp>
    <Devices></Devices>
    <UserDeviceTree></UserDeviceTree>
    <UniversalTree></UniversalTree>
    <Images></Images>
    <Icons></Icons>
    <Fonts></Fonts>
    <Sounds></Sounds>
    <Scripts></Scripts>
    <ScriptModules></ScriptModules>
    <RelationsTags></RelationsTags>
    <Effects></Effects>
    <Pages></Pages>
    <Popups></Popups>
    <Folders></Folders>
    <VirtualTags></VirtualTags>
    <Macros></Macros>
</Panel>
```

## Panel
Корневой элемент структуры, в аттрибутах которого указываются параметры отображения проекта (разрешение, ориентация) и общая информация о проекте
```
<Panel 
    AddressInfo="" 
    CustomerInfo="" 
    DefaultOrientation="0" 
    DescriptionInfo="" 
    EditorVersion="2.2.3.17853" 
    EmailInfo="" 
    Height="768" 
    LandscapeHeight="768" 
    LandscapeWidth="1024" 
    NameInfo="" 
    PhoneInfo="" 
    PortraitHeight="1024" 
    PortraitWidth="768" 
    ResolutionName="" 
    Rotate="0" 
    Version="2.0" 
    Width="1024">
</Panel>
```

## StartUp
Определяет страницу, которая отображается при запуске проекта на панели управления, и открытые элементы Popup. А также страницу, являющуюся screensaver'ом, и время перехода на нее в случае бездействия со стороны пользователя
```
    <StartUp Page="MainPage" Sleep="MainPage" Time="0">
        <Popup Name="common#Background"/>
    </StartUp>
```

## Devices
В данном элементе перечисляются устройства с которыми взаимодействует панель управления, настройки для связи с ними, описание команд и сигналов обратной связи
```
    <Devices>
        <Device Host="127.0.0.1" ID="1" Name="NI4100" Parameters="10001" Port="1319" Type="01">
            <Channels>
                <Channel Data="01, 01, 01, 01, 00, 05, 00, 00" ID="1" Name="PoolLight1" Size="8"/>
                <Channel Data="01, 01, 02, 01, 00, 05, 00, 00" ID="8" Name="PoolMainBlind" Size="8"/>
                ...
            </Channels>
            <Tags>
                <Tag Data="02, 01, 01, 00" ID="1" Name="PoolLight1" Size="4"/>
                <Tag Data="02, 02, 01, 00" ID="8" Name="PoolMainBlind" Size="4"/>
                ...
            </Tags>
        </Device>
    </Devices>
```
Аттрибуты элементов **Device**, **Channel** и **Tag** зависят от типа устройства. Ниже приведены примеры значений для контроллера AMX:  

**Device**  
*Parameters* - PanelID  
*Host* - Host  
*Port* - Port   

**Channel**    
Комманда управления  
*Data[0]* - Channel Type (00 - Channel, 01 - Level, 02 - String, 03 - Command)  
*Data[1]* - Data (00 - Internal, 01 - External)  
*Data[2]* - Port  
*Data[3]* - Code  
*Data[4..]* - Описание данных при фиксированной комманде (Internal)  

**Tag**    
Сигнал обратной связи  
*Data[0]* - Channel Type (00 - Address, 01 - Channel, 02 - Level)  
*Data[1]* - Port  
*Data[2]* - Code  

## UserDeviceTree
Похоже на описание того, как используются устройства в проекте... Количество элементов **Device** и **Tag** для конкретного устройства должно соответствовать количеству комманд *Commands* и сигналов обратной связи *Feedback* в проекте *Iridium*
```
    <UserDeviceTree>
        <Device ControllerType="0" Description="" Direction="1" Name="TCP Gateway" Type="3">
            <Channel Description="" Direction="2" ExternalDataType="1" InputOutputType="5" Name="Gateway out" NotUseHeader="0" Sequence="" Type="1">
                <Device ControllerType="0" Description="" Direction="1" Name="NI4100" Type="4">
                    <Input Description="" Direction="3" ExternalDataType="1" InputOutputType="9" Name="Хостовый Вход" NotUseHeader="0" Sequence="" Type="1">
                        <Option DefaultValue="127.0.0.1" Description="" Name="Host" NotUse="0" NotVisible="0" Number="1" Type="6" Value="127.0.0.1"/>
                        ...
                    </Input>
                    <Channel Description="" Direction="2" ExternalDataType="1" InputOutputType="21" Name="Virtual" NotUseHeader="0" Sequence="" Type="9">
                        <Option DefaultValue="Do not use." Description="" Name="Host" NotUse="0" NotVisible="0" Number="1" Type="6" Value="Do not use."/>
                        ...
                        <Device ChannelID="10001" ControllerType="6" Description="" Direction="1" Name="PoolLight1" Type="5">
                            <Input Description="" Direction="3" ExternalDataType="1" InputOutputType="22" Name="HostInput" NotUseHeader="0" Sequence="" Type="9"/>
                        </Device>
                        ...
                        <Tag ChannelID="10001" ControllerType="6" Description="" Direction="1" Name="PoolLight1" Type="5">
                            <Input Description="" Direction="3" ExternalDataType="1" InputOutputType="22" Name="HostInput" NotUseHeader="0" Sequence="" Type="9"/>
                        </Tag>
                        ...
                    </Channel>
                </Device>
            </Channel>
        </Device>
    </UserDeviceTree>
```

## UniversalTree
Список комманд *Commands* (**Type="5"**) и сигналов обратной связи (**Type="6"**). Количество элементов с аттрибутом **Type="5"** и аттрибутом **Type="6"** для конкретного устройства должно соответствовать количеству комманд *Commands* и сигналов обратной связи *Feedback* в проекте *Iridium*
```
    <UniversalTree>
        <Node Driver="NI4100" Name="PoolLight1" Type="5"/>
        <Node Driver="NI4100" Name="PoolLight2" Type="5"/>
        ...
        <Node Driver="NI4100" Name="PoolLight1" Type="6"/>
        <Node Driver="NI4100" Name="PoolLight2" Type="6"/>
        ...
    </UniversalTree>
```

## Images
Перечисление используемых в проекте изображений, которые отмечаются в поле *Image* вкладки *States* элементов управления
```
    <Images>
        ...
        <Sprite ID="2" Name="down_button.png" Type="1"/>
        <Sprite ID="3" Name="down_button_push.png" Type="1"/>
        ...
    </Images>
```
Значение аттрибута **ID** используется в аттрибуте **ImageID** элемента **Popup**->**Item**->**State**->**Canvas**  

## Icons
Перечисление используемых в проекте изображений, которые отмечаются в поле *Icon* вкладки *States* элементов управления
```
    <Icons>
        ...
        <Sprite ID="2" Name="lamp_on.png" Type="1"/>
        <Sprite ID="3" Name="lamp_off.png" Type="1"/>
        ...
    </Icons>
```
Значение аттрибута **ID** используется в аттрибуте **IconID** элемента **Popup**->**Item**->**State**->**Canvas**  

## Fonts
Перечисление используемых в проекте шрифтов
```
    <Fonts>
        ...
        <Font File="ariblk.ttf" ID="3" Name="Arial Black" Size="8" Weights="0"/>
        <Font File="segoeui.ttf" ID="4" Name="Segoe UI" Size="12" Weights="0"/>
        ...
    </Fonts>
```
Значение аттрибута **ID** используется в аттрибуте **FontID** элемента **Popup**->**Item**->**State**->**Text**  

## Sounds
Перечисление используемых в проекте аудиофайлов
```
    <Sounds>
        <Sound File="summer.mp3" ID="1" Name="summer.mp3"/>
        ...
    </Sounds>
```

## Scripts
Перечисление файлов, содержащих функции JavaScript
```
    <Scripts>
        <Script File="Main.js" Name="Main"/>
        ...
    </Scripts>
```

## ScriptModules

## RelationsTags
Выражения типа *DestinationValue=SourceValue*. Например, описание связей сигналов обратной связи от оборудования, непосредственно влияющие на состояние элементов управления панели, либо связей между состояниями самих элементов управления
```
    <RelationsTags>
        <RelationTags LHS="Drivers.NI4100.PoolDimLight" RHS="UI.common#dim_popup.DimCaption.Value"/>
        <RelationTags LHS="Drivers.NI4100.PoolDimLight" RHS="UI.common#dim_popup.DimSlider1.Value"/>
        ...
    </RelationsTags>
```
Аттрибут **RHS** соответствует *DestinatonValue*, а аттрибут **LHS** - *SourceValue*  

## Effects
Перечисление эффектов открытия элементов Popup, которые используются в проекте
```
    <Effects>
        <Effect Delay="0" Duration="300" Group="1" TweenType="0" Type="fade"/>
        ...
    </Effects>
```
Значение аттрибута **Group** используется в аттрибуте **SchowEffect** или **HideEffect** элемента **Popup**  

## Pages
В данном контейнере описываются страницы (элементы Page) проекта
```
    <Pages>
        <Page Name="MainPage" Orientation="0" Type="0">
            <ActionCases/>
            <State>
                <Canvas ChameleonFiltration="0" ChameleonStretch="0" Color="#C0C0C0FF" IconAlign="5" IconFiltration="0" IconStretch="0" ImageAlign="5" ImageFiltration="0" ImageStretch="0" Opacity="255" Order="#34521"/>
                <Text Align="5" Color="#00000000" FontID="1" WordWrap="0"/>
                <Template Color="#FFFFFFFF" ID="0"/>
            </State>
        </Page>
        ...
    </Pages>
```
**Аттрибуты элемента Page** - соответствуют вкладке *General* свойств страницы  
**ActionCases** - описывает реакцию на действия пользователей (жесты, нажатия)  
**State** - описание вкладки *States* свойств страницы

## Popups
В данном контейнере описываются элементы Popup (всплывающие окна) проекта
```
    <Popups>
        ...
        <Popup Drop="0" HideEffect="4" Name="common#screenoff_popup1" Orientation="0" ShowEffect="3" Timeout="0">
            <Rect H="768" W="1024" X="0" Y="0"/>
            <State>
                <Canvas ChameleonFiltration="0" ChameleonStretch="0" Color="#000000FF" IconAlign="5" IconFiltration="0" IconStretch="0" ImageAlign="5" ImageFiltration="0" ImageStretch="0" Opacity="255" Order="#34521"/>
                <Text Align="5" Color="#000000FF" FontID="1" WordWrap="0"/>
                <Template Color="#FFFFFFFF" ID="0"/>
            </State>
            <Item DefaultValue="0" Feedback="4" Group="0" Hit="2" HoldTime="500" Locked="0" Name="betoo_logo_big" PasswordID="0" RepeatTime="250" SoundID="0" Type="0">
            </Item>
            <Item DefaultValue="0" Feedback="4" Group="0" Hit="1" HoldTime="500" Locked="0" Name="pressArea" PasswordID="0" RepeatTime="250" SoundID="0" Type="0">
            </Item>
        </Popup>
        ...
    </Popups>
```
**Аттрибуты элемента Popup** и элемент **Rect** - соответствуют вкладке *General* свойств всплывающего окна  
**ActionCases** - описывает реакцию на действия пользователей (жесты, нажатия)  
**State** - описание вкладки *States* свойств всплывающего окна  
**Item** - Элемент управления, расположенный в данном всплывающем окне(описание аналогично элементу Popup):
```
            <Item DefaultValue="0" Feedback="4" Group="0" Hit="1" HoldTime="500" Locked="0" Name="pressArea" PasswordID="0" RepeatTime="250" SoundID="0" Type="0">
                <Rect H="768" W="1024" X="0" Y="0"/>
                <Animation Repeate="0" TimeDown="100" TimeUp="100"/>
                <ActionCases>
                    <ActionCase Event="press">
                        <Action Param="screenOnByClick" Type="script_call"/>
                    </ActionCase>
                </ActionCases>
                <State>
                    <Canvas ChameleonFiltration="0" ChameleonStretch="0" Color="#3E9EFFFF" IconAlign="5" IconFiltration="0" IconStretch="0" ImageAlign="5" ImageFiltration="0" ImageStretch="0" Opacity="0" Order="#34521"/>
                    <Text Align="5" Color="#000000FF" FontID="1" WordWrap="0"/>
                    <Template Color="#FFFFFFFF" ID="0"/>
                </State>
            </Item>
```

## Folders
Описание структуры проекта по папкам в *Project Overview*:
```
    <Folders>
        <Branch Name="Zhukovka-pool1" Type="1">
            <Branch Name="common#Background" Orientation="0" Type="4"/>
            <Branch Name="common#screenoff_popup1" Orientation="0" Type="4"/>
            <Branch Name="common#screenoff_popup2" Orientation="0" Type="4"/>
            <Branch Name="Pool" Type="7">
                <Branch Name="pool#pool_main_popup" Orientation="0" Type="4"/>
                <Branch Name="pool_scenario_pages" Type="7">
                    <Branch Name="pool#pool_popcorn_video_sc" Orientation="0" Type="4"/>
                    <Branch Name="pool#pool_radio_sc" Orientation="0" Type="4"/>
                    <Branch Name="pool#pool_tv_sc" Orientation="0" Type="4"/>
                    <Branch Name="pool#pool_popcorn_audio_sc" Orientation="0" Type="4"/>
                </Branch>
            </Branch>
            ...
            <Branch Name="MainPage" Orientation="0" Type="4"/>
            ...
        </Branch>
    </Folders>
```
Аттрибут **Type** определяет - является ли элемент **Branch** папкой (7) или это конечный элемент (Popup или Page) (4) 

## VirtualTags
Глобальные переменные проекта (*Project Tokens*)
```
    <VirtualTags>
        <VirtualTag DefaultValue="40" Folder="0" Name="PoolVolume" UseDefaultValue="1"/>
        <VirtualTag DefaultValue="0" Folder="0" Name="PoolMute" UseDefaultValue="1"/>
        ...
    </VirtualTags>
```
Если переменные создаются посредством JavaScript, то описывать их в данной структуре необязательно  

## Macros
Описание макросов, которые входят в состав *Project Gallery* проекта
```
    <Macros>
        <Item Name="macros1">
            <Action Name="macros1" Param="barbershop#barbershop_popcorn_audio_sc" Type="show_popup"/>
            <Action Loop="0" Name="macros1" Slot="0" SoundID="1" Type="play_sound" Volume="50"/>
        </Item>
        ...
    </Macros>
```

# Project Example (.irpz-file)
[Source file of Iridium Project](https://bitbucket.org/estbeetoo/irp_structure/raw/master/project.irpz)

# Project Example in XML (.irp-file)
[XML file of Iridium Project](https://bitbucket.org/estbeetoo/irp_structure/raw/master/Project.irp)

# Copyrights

BeeToo, Yury Kornilov